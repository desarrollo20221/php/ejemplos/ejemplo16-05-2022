<?php

    function actionIndex() {
        render("index", []);
    }

    function actionContacto() {
        // es la primera vez que cargo el formulario
        if(!isset($_GET["boton"])){
            render("formulario", []);
        } else {
            // cuando he pulsado el boton
            render ("mensaje",[]);
        }
        
    }

    function actionConocenos() {
        render("conocer", [
            "titulo" => "Empresa de formacion actual",
            "descripcion" => "descripcion de la empresa",
            "foto" => "empresa.jpg" // no se muestra todavia
        ]);
    }

    function actionQuienes() {
        $componentes = [
            "alumno1", "alumno2", "alumno3", "Jose", "Ana"
        ];
        render("equipo", [
            "equipo" => $componentes,
        ]);
    }

    function actionDonde() {
        render("mapa", []);
    }
