<?php
    // funciones de mi aplicacion
    function render($vista,$parametros){
        extract($parametros); // crear variable desde un array asociativo
        include "./views/$vista.php";
    }

    // cargo el controlador por defecto
    require "controller/siteController.php";
    
    /**
     * Todo esto es para saber que accion tiene que ejecutar
     */
    
    // gestiono la accion del controlador a ejecutar
    $urlCompleta = $_SERVER["HTTP_HOST"] . $_SERVER["REQUEST_URI"];
    
    // cuando el form envie datos los elimino de la url
    $separoParametros=explode("?",$urlCompleta);
    
    // me quedo con la url sin parametros
    $url=$separoParametros[0]; // aqui tengo la url
    // mi url seria algo asi
    // http://localhost/ejemplos/index.php/accion
    
    // me quedo con el texto que viene detras de la ultima "/" de la url (accion)    
    $accion=substr($url,strrpos($url, "/")+1);
    
    // establezco la accion por defecto la accion la primera vez que carga la aplicacion
    if($accion=="index.php"){       
        $accion="index";
    }
    
    // en esta aplicacion todas las acciones tienen que tener el prefijo action
    $accion="action" . ucfirst($accion);

    /**
     * Fin de saber que accion ejecutar
     */