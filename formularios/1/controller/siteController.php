<?php
  
    function actionSalida(){
        render("mostrar",[
            "nombre" => $_GET["nombre"]
        ]);
    }
    
    function actionEntrada(){
        render("formulario",[
            "nombre" => ""
        ]);
    }
    
    function actionFormulario(){
        if(isset($_GET["boton"])){
            actionSalida();
        }else{
            actionEntrada();
        }
    }
    
    function actionIndex(){
        render("index",[]);
    }


