<!DOCTYPE html>
<html>
    <head>
        <meta charset="UTF-8">
        <title></title>
    </head>
    <body>
        <?php
        // compruebo si he pulsado el boton
        if (isset($_GET["boton"])) {
            $nombre = $_GET["nombre"];
            include './mostrar.php'; // vista para mostrar resultados
        } else {
            // el formulario carga por primera vez
            $nombre = "";
            include './formulario.php'; // vista mostar formulario
        }
        ?>
    </body>
</html>
